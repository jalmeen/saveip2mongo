package sample;

public class InfoToBeSaved {

    private String deskNumber;
    private String macAddr;

    public InfoToBeSaved(){
        this.deskNumber = null;
        this.macAddr = null;
    }
    public void setDeskNumber(String deskNumber){
        this.deskNumber = deskNumber;
    }

    public String getDeskNumber() {
        return deskNumber;
    }

    public String getMacAddr() {
        return macAddr;
    }

    public void setMacAddr(String macAddr) {
        this.macAddr = macAddr;
    }

    @Override
    public String toString() {
        return "InfoToBeSaved{" +
                "deskNumber='" + deskNumber + '\'' +
                ", macAddr='" + macAddr + '\'' +
                '}';
    }
}
