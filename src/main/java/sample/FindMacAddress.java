package sample;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class FindMacAddress {

    private byte[] bytes;
    private NetworkInterface networkInterface;
    private Enumeration<NetworkInterface> interfaces;
    private StringBuilder sb;

    public FindMacAddress() {
        this.bytes = null;
        this.sb = null;
        this.interfaces = null;
        this.networkInterface = null;
    }

    public FindMacAddress(byte[] bytes, NetworkInterface networkInterface, Enumeration<NetworkInterface> interfaces, StringBuilder sb) {
        this.bytes = bytes;
        this.networkInterface = networkInterface;
        this.interfaces = interfaces;
        this.sb = sb;
    }

    public void displayMacAddress() throws SocketException {

        interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {

            networkInterface = interfaces.nextElement();
            bytes = networkInterface.getHardwareAddress();

            sb = new StringBuilder();

            if (bytes != null) {
                for (byte b : bytes) {
                    sb.append(String.format("%1$02X ", new Byte(b)));
                }
            }
            System.out.println(sb);
        }
    }
}
