package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.SocketException;

public class Controller {

    private int deskNumber;
    @FXML private Button confirmBtn;
    @FXML private TextField enterNumber;

    @FXML protected int handleConfirmBtnClick(ActionEvent actionEvent) throws SocketException {
        deskNumber = Integer.parseInt(this.enterNumber.getText());
        if(deskNumber <= 60) {
            System.out.println(deskNumber);
            System.out.println("Confirm button clicked by system number " + deskNumber);

            FindMacAddress  findMacAddress = new FindMacAddress();
            findMacAddress.displayMacAddress();
        }

        else {
            System.out.println("Out of range");
        }

        enterNumber.clear();
        return deskNumber;
    }
}
